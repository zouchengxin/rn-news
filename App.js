import 'react-native-gesture-handler';
import React from 'react';
import { StyleSheet, Text, View, useColorScheme,StatusBar } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Navigation from './navigation/index';

function App() {
  const colorScheme = useColorScheme();
  
  return (
    <SafeAreaProvider>
      <StatusBar />
      <Navigation colorScheme={colorScheme} />
    </SafeAreaProvider>
  );
}

const styles=StyleSheet.create({
  navContainer:{
    paddingTop:40,
  }
})

export default App;
