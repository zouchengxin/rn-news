import React from "react";
import { View, StyleSheet,ScrollView } from "react-native";
import {getHotVideos} from '../services/hot';
import HotItem from "../components/HotItem";

class HotsTabScreen extends React.Component{
  constructor(props){
    super(props);
    this.state={
      hots:[]
    }
  }
  componentDidMount() {
    getHotVideos().then(res =>{
      console.log('getHotVideos:',res);
      this.setState({
        hots:res
      });
    })
    
  }
  render(){
    return (
      <>
        <ScrollView style={styles.listWrapper}>
          {(this.state as any).hots.map((item,index) =><HotItem item={item} key={index} />)}
        </ScrollView>
      </>
    );
  }
}
const styles=StyleSheet.create({
  listWrapper:{
    flexGrow:1,
    padding:10
  }
})
export default HotsTabScreen;