import * as React from 'react';
import { StyleSheet,Text, View,ToastAndroid,Platform } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input, Button  } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';

function LoginScreen(){
  const navigation = useNavigation();
  const [userName,setUserName]=React.useState('');
  const [passWord,setPassWord]=React.useState('');
  const login=() =>{
    if(userName=='admin'&&passWord=='admin'){
      if(Platform.OS=='android') ToastAndroid.show("登陆成功", ToastAndroid.SHORT);
      navigation.navigate('Tab',{screen:'News'});
    }else{
      if(Platform.OS=='android') ToastAndroid.show("登陆失败", ToastAndroid.SHORT);
    }
  }
  return (
      <View style={styles.loginScreen}>
        <View style={styles.loginForm}>
          <Input
            placeholder='请输入用户名'
            label='用户名'
            leftIcon={
              <Icon
                name='user'
                size={24}
                color='black'
              />
            }
            onChangeText={val => setUserName(val)} 
          />
          <Input 
            placeholder='请输入密码'
            label='密码' 
            leftIcon={
              <Icon
                name='key'
                size={24}
                color='black'
              />
            }
            secureTextEntry={true}
            onChangeText={val => setPassWord(val)} 
          />
          <Button
            title='登录'
            onPress={login}
            containerStyle={styles.submitBtn}
          />
        </View>
      </View>
  );
}

const styles=StyleSheet.create({
  loginScreen:{
    width:'100%',
    height:'100%',
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
  },
  loginForm:{
    width:'100%',
    height:300,
    maxWidth:400,
    display:'flex',
    flexDirection:'column',
    justifyContent:'space-around',
    alignItems:'center',
    padding:20,
  },
  submitBtn:{
    height:50,
    width:'80%'
  }
});

export default LoginScreen;