import React from "react";
import { View, StyleSheet,ScrollView } from "react-native";
import { Tab } from 'react-native-elements';
import NewsList from '../components/NewsList';

function NewsTab(){
  const [tabIndex,setTabIndex]=React.useState(0);
  const [newsType,setNewsType]=React.useState('top');
  const tabs=[
    {
      label:'推荐',
      value:'top'
    },
    {
      label:'国内',
      value:'guonei'
    },
    {
      label:'娱乐',
      value:'yule'
    },
    {
      label:'体育',
      value:'tiyu'
    },
    {
      label:'军事',
      value:'junshi'
    },
    {
      label:'科技',
      value:'keji'
    },
    {
      label:'财经',
      value:'caijing'
    },
  ]

  return (
    <>
      <ScrollView style={styles.tabWrapper} horizontal={true}>
        <View style={styles.tabs} >
          <Tab
            variant='primary'
            indicatorStyle={{
              borderBottomColor:'white',
              borderBottomWidth:5
            }} 
            value={tabIndex}
            onChange={(index) =>{
              setTabIndex(index);
              setNewsType(tabs[index].value);
            }}
          >
            {tabs.map(item => <Tab.Item title={item.label} key={item.value} />)}
          </Tab>
        </View>
      </ScrollView>
      <View style={styles.listWrapper}>
        <NewsList type={newsType} key={newsType} />
      </View>
    </>
  )
}

const styles=StyleSheet.create({
  tabWrapper:{
    overflow:'scroll',
    flexGrow:0
  },
  tabs:{
    width:600,

  },
  listWrapper:{
    flexGrow:1,
    margin:10
  }
})

export default NewsTab;