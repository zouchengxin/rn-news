import React from "react";
import { View,Text,StyleSheet } from "react-native";
import { useRoute } from '@react-navigation/native';
import { getNewsDetail } from "../services/api";
import { WebView } from 'react-native-webview';
import Layout from "../constants/Layout";

function NewsDetailSreen(){
  const route:any = useRoute();
  const [detail,setDetail]=React.useState({});

  React.useEffect(() =>{
    getNewsDetail(route.params.id).then(res =>{
      //console.log(res);
      setDetail(res);
    })
  },[]);
  return (
    <WebView
      source={{ uri:(detail as any)?.detail?.url||'https://expo.io' }}
      style={styles.content}
    />
  )
}

const styles=StyleSheet.create({
  container:{
    backgroundColor:"white",
    padding:10,
    width:Layout.window.width
  },
  title:{
    fontSize:21,
    fontWeight:"800",
    lineHeight:28,
    color:"#222",
  },
  info:{
    color: "#999",
    fontSize:12,
    padding:10
  },
  author:{
    
  },
  date:{

  },
  content:{
    width:Layout.window.width,
    height:Layout.window.height,
    padding:10
  }
})
export default NewsDetailSreen;