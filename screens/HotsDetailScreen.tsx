import { useRoute } from "@react-navigation/native";
import React from "react";
import { View, StyleSheet } from "react-native";
import { WebView } from 'react-native-webview';
import Layout from "../constants/Layout";

class HotsDetailScreen extends React.Component<any>{
  constructor(props){
    super(props);
  }
  render(){
    return (
      <WebView
        source={{ uri:this.props.route.params.url||'https://expo.io' }}
        style={styles.content}
      />
    );
  }
}

const styles=StyleSheet.create({
  content:{
    width:Layout.window.width,
    height:Layout.window.height,
    padding:10
  }
});

export default function(props) {
  const route = useRoute();

  return <HotsDetailScreen {...props} route={route} />;
}