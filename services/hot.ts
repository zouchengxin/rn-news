import {Platform} from 'react-native'
const baseUrl='http://apis.juhe.cn';
const key='3f74c961d5cf6ec2c13b1f26427619e7';

const hotVideos=[
  {
    "title": "宁波黑坑水库盘老板，看看最后的渔获在你们那里能值多少钱。 #钓鱼  #dou来钓鱼516",
    "share_url": "https://www.iesdouyin.com/share/video/6960674964378897677/?region=CN&mid=6960676544603851533&u_code=0&titleType=title&did=MS4wLjABAAAANwkJuWIRFOzg5uCpDRpMj4OX-QryoDgn-yYlXQnRwQQ&iid=MS4wLjABAAAANwkJuWIRFOzg5uCpDRpMj4OX-QryoDgn-yYlXQnRwQQ&with_sec_did=1",
    "author": "天元邓刚",
    "item_cover": "https://p6.douyinpic.com/img/tos-cn-p-0015/cd3cd955c1ec40d8a346c7fc652db36f~c5_300x400.jpeg?from=2563711402_large",
    "hot_value": 188721410,
    "hot_words": "老板,最后的,钓鱼,宁波,黑坑,水库,看看,渔获,你们,那里,能值,多少,dou,516",
    "play_count": 303819638,
    "digg_count": 2209478,
    "comment_count": 97122
  }
]
function getHotVideos(){
  const defaultVal=[1,2,3,4,5,6].map(item =>hotVideos[0]);
  if(Platform.OS=='web'){
    return new Promise((resove,reject) =>{
      setTimeout(() =>resove(defaultVal),2000);
    });
  }
  return fetch(`${baseUrl}/fapig/douyin/billboard?key=${key}&type=hot_video&size=50`, {
    method: 'POST',
    mode: "cors",
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  }).then((response) => response.json()).then((json:any) => {
    if(json.error_code==0){
      return Promise.resolve(json.result);
    }else{
      return Promise.resolve(defaultVal);
      //return Promise.reject(json);
    }
  }).catch(err => Promise.resolve(defaultVal));
}

export {
  getHotVideos
}