import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screens/LoginScreen';
import BottomTabsNavigation from './bottomTabs';
import NewsDetailSreen from '../screens/newsDetailSreen';
import HotsDetailScreen from '../screens/HotsDetailScreen';

const Stack = createStackNavigator();
function RootNavigation(){
  return (
    <Stack.Navigator
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen 
        name="Login" 
        component={LoginScreen} 
      />
      <Stack.Screen 
        name="NewsDetail" 
        options={{
          headerShown:true,
          headerTitle:'新闻详情',
        }}
        component={NewsDetailSreen} 
      />
      <Stack.Screen 
        name="HotsDetail" 
        options={{
          headerShown:true,
          headerTitle:'热门详情',
        }}
        component={HotsDetailScreen} 
      />
      <Stack.Screen name="Tab" component={BottomTabsNavigation} />
    </Stack.Navigator>
  );
}

export default RootNavigation;