import React from 'react';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { ColorSchemeName,StyleSheet } from 'react-native';
import RootNavigation from './Root';

function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }){
  return (
    <NavigationContainer
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}
    >
      <RootNavigation />
    </NavigationContainer>
  );
}

export default Navigation;