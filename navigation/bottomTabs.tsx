import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import NewsTabScreen from '../screens/NewsTabScreen';
import HotsTabScreen from '../screens/HotsTabScreen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

const Tab = createBottomTabNavigator();
function BottomTabsNavigation(){
  return (
    <Tab.Navigator>
      <Tab.Screen 
        name="News" 
        component={NewsTabScreen} 
        options={{
          title: '新闻',
          tabBarLabel:'新闻',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="newspaper" color={color} size={size} />
          ),
        }} />
        <Tab.Screen 
          name="Hots" 
          component={HotsTabScreen} 
          options={{
            title: '热门视频',
            tabBarLabel:'热门视频',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="video-account" color={color} size={size} />
            ),
          }} />
    </Tab.Navigator>
  );
}

export default BottomTabsNavigation;