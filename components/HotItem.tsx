import { useNavigation } from "@react-navigation/native";
import React from "react";
import { View, StyleSheet,Text, TouchableOpacity,Platform,Image } from "react-native";
import defaultImage from '../assets/default.jpg';

class HotItem extends React.Component<any>{
  constructor(props){
    super(props);
    console.log(props.navigation);
  }
  numConvert(num){
    if(num>=10000){
      num=Math.round(num/1000)/10+'W';
    }else if(num>=1000){
      num=Math.round(num/100)/10+'K';
    }
    return num;
  }
  onPress(url){
    this.props.navigation.navigate('HotsDetail',{url});
  }
  render(){
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={e => this.onPress(this.props.item.share_url)}
      >
        <View style={styles.leftWrapper}>
          <Text style={styles.title} numberOfLines={2}>{this.props.item.title}</Text>
          <Text style={styles.author}>{this.props.item.author}</Text>
          <Text style={styles.info}>
            <Text>播放数:{this.numConvert(this.props.item.play_count)}</Text>
            <Text>   </Text>
            <Text>点赞数:{this.numConvert(this.props.item.digg_count)}</Text>
            <Text>   </Text>
            <Text>评论数:{this.numConvert(this.props.item.comment_count)}</Text>
          </Text>
        </View>
        <View style={styles.rightWrapper}>
          <Image 
            source={Platform.OS=='web'?defaultImage:{ uri: this.props.item.item_cover }}
            style={styles.image}
          />
        </View>
        
      </TouchableOpacity>
    );
  }
}
const styles=StyleSheet.create({
  itemContainer:{
    padding:10,
    marginBottom:10,
    display:"flex",
    flexDirection:"row",
    flexWrap:"nowrap",
    alignItems:"center",
    backgroundColor:"white",
    borderRadius:10
  },
  leftWrapper:{
    flexBasis:'65%',
    flexGrow:0,
    flexShrink:0
  },
  rightWrapper:{
    flexBasis:'35%',
    flexGrow:0,
    flexShrink:0
  },
  title:{
    fontSize:16,
  },
  info:{
    marginTop:10,
    marginBottom:10,
    color:'#999',
    fontSize:8
  },
  author:{
    color:'#999',
    marginTop:10
  },
  image:{ 
    width: "90%", 
    height:100,
    resizeMode: 'contain',
    margin:5
  }
})
export default function(props) {
  const navigation = useNavigation();
  return <HotItem {...props} navigation={navigation} />;
};