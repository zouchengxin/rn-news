import * as React from 'react';
import { View,Text,FlatList,Platform,StyleSheet, ActivityIndicator } from 'react-native';
import NewsItem from './NewsItem';
import * as Api from '../services/api';
import Layout from '../constants/Layout';

// function NewsList({type}){
//   const [news,setNews]=React.useState([]);
//   const page={
//     pageNum:1,
//     pageSize:5
//   }
//   const ref=React.useRef(null);
//   const [loading,setLoading]=React.useState(false);
//   const [isMore,setIsMore]=React.useState(true);
//   const getNewsData=() =>{
//     setLoading(true);
//     return Api.getNewsList(type,page.pageNum,page.pageSize).then(res => {
//       console.log((res as any).length,ref.current.length);
//       setNews(ref.current.concat(res) as Array<any>);
//       setLoading(false);
//     });
//   }
//   React.useEffect(() =>{
//     page.pageNum=1;
//     setNews([]);
//     getNewsData();
//     setIsMore(true);
//   },[type]);

//   React.useEffect(() =>{
//     ref.current=news;
//   },[news]);
  
//   return (
//     <FlatList
//       style={styles.listContainer}
//       data={news}
//       renderItem={({item}) =>{
//         return (
//           <NewsItem item={item} />
//         )
//       }}
//       keyExtractor={(item,index) => Platform.OS=='web'?`${index}`:item.uniquekey}
//       onRefresh={() =>{
//         console.log('onRefresh');
//         setNews([]);
//         page.pageNum=1;
//         getNewsData();
//       }}
//       onEndReached={(info) =>{
//         console.log('onEndReached:',page.pageNum);
//         if(page.pageNum>5){
//           setIsMore(false);
//         }else{
//           page.pageNum++;
//           getNewsData();
//         }
//       }}
//       onEndReachedThreshold={0.1}
//       refreshing={loading}
//       ListFooterComponent={() => 
//         isMore?<ActivityIndicator size="large" style={styles.loadingMore}/>:<Text>没有数据了</Text>}
//     />
//   );
// }
const styles=StyleSheet.create({
  listContainer:{
    height:Layout.window.height-56-48-20
  },
  noMore:{
    textAlign:'center'
  }
})

class NewsList extends React.Component<{type}>{
  constructor(props) {
    super(props);
    this.state = {
      isRefreshing:false,
      hasMore:true,
      news:[]
    };
  }
  componentDidMount() {
    (this as any).page={
      pageNum:1,
      pageSize:5
    };
    (this as any).isLoading=false;
    this.getNewsData();
  }

  componentWillUnmount() {
  }
  render(){
    return (
      <FlatList
      style={styles.listContainer}
      data={(this.state as any).news}
      renderItem={({item}) =>{
        return (
          <NewsItem item={item} />
        )
      }}
      keyExtractor={(item,index) => `${index}`}
      onRefresh={this.onRefreshList}
      onEndReached={this.onEndReachedList}
      onEndReachedThreshold={0.1}
      refreshing={(this.state as any).isRefreshing}
      ListFooterComponent={() => 
        (this.state as any).hasMore?<ActivityIndicator size="large"/>:<Text style={styles.noMore}>没有更多数据了</Text>}
    />
    );
  }

  onEndReachedList=(info) =>{
    console.log('onEndReached')
    if((this as any).isLoading) return;
    if((this as any).page.pageNum>5){
      this.setState({
        hasMore:false
      });
    }else{
      (this as any).page.pageNum++;
      this.getNewsData();
    }
  }

  onRefreshList=() =>{
    console.log('onRefresh');
    (this as any).page.pageNum=1;
    this.setState({
      isRefreshing:true
    });
    this.getNewsData();
  }

  getNewsData(){
    (this as any).isLoading=true;
    return Api.getNewsList((this.props as any).type,(this as any).page.pageNum,(this as any).page.pageSize).then(res => {
      this.setState((state,props) => ({
        news:(state as any).news.concat(res),
        isRefreshing:false,
      }),() =>{
        (this as any).isLoading=false;
        console.log((res as any).length,(this.state as any).news.length);
      })
    });
  }
}
export default NewsList;