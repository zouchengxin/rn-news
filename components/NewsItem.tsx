import React from "react";
import { TouchableOpacity,Text,StyleSheet,View,Platform } from "react-native";
import { Image } from 'react-native-elements';
import defaultImage from '../assets/default.jpg';
import { useNavigation } from '@react-navigation/native';

function NewsItem({item}){
  const navigation = useNavigation();
  const onPress=() =>{
    navigation.navigate('NewsDetail',{id:item.uniquekey})
  };
  return (
    <TouchableOpacity
      style={styles.itemContainer}
      onPress={onPress}
    >
      <View style={styles.leftWrapper}>
        <Text style={styles.title} numberOfLines={2}>{item.title}</Text>
        <Text style={styles.info}>
          <Text style={styles.category}>{item.category}</Text>
          <Text>   </Text>
          <Text style={styles.author}>{item.author_name}</Text>
        </Text>
        <Text style={styles.date}>{item.date}</Text>
      </View>
      <View style={styles.rightWrapper}>
        <Image 
          source={Platform.OS=='web'?defaultImage:{ uri: item.thumbnail_pic_s }}
          style={styles.image}
        />
      </View>
      
    </TouchableOpacity>
  );
}
const styles=StyleSheet.create({
  itemContainer:{
    padding:10,
    marginBottom:10,
    display:"flex",
    flexDirection:"row",
    flexWrap:"nowrap",
    alignItems:"center",
    backgroundColor:"white",
    borderRadius:10
  },
  leftWrapper:{
    flexBasis:'65%',
    flexGrow:0,
    flexShrink:0
  },
  rightWrapper:{
    flexBasis:'35%',
    flexGrow:0,
    flexShrink:0
  },
  title:{
    fontSize:16,
  },
  info:{
    marginTop:10,
    marginBottom:10,
    color:'#999'
  },
  category:{
    
  },
  author:{

  },
  date:{

  },
  image:{ 
    width: "90%", 
    height:100,
    resizeMode: 'contain',
    margin:5
  }
})
export default NewsItem;