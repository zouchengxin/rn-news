### 1.目录结构
- android           android平台打包目录
- assets            资源目录
- components        组件目录
- constants         保存常量
- navigations       导航组件(Tab,Botton)
- screens           页面组件
- service           接口请求
### 2.功能
- 登录(账号:admin 密码:admin)
- 新闻列表(下拉刷新,上拉加载,顶部tab切换)
- 新闻详情
- 热门视频
- 热门视频详情